package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MainActivity extends AppCompatActivity {
    TextView txtInput;
    TextView txtHistory;
    Button btnNumber;
    Button btnOperation;
    String numberOne = "";
    String numberTwo = "";
    String operationString;
    String resultadoFinal;
    final String defaultValue = "0";

    List<String> operationsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtHistory = findViewById(R.id.txtViewHistory);
        txtInput = findViewById(R.id.txtViewInput);
        operationsList = getOperationsList();
        txtInput.setText(defaultValue);
    }

    public void onClickNumber(View view){
        btnNumber = (Button) view;
        String inputNum = btnNumber.getText().toString();
        String stringHistory = txtHistory.getText().toString();

        if(stringHistory.isEmpty()){
            numberOne += inputNum;
            txtInput.setText(numberOne);
        }else{
            numberTwo += inputNum;
            txtInput.setText(numberTwo);
        }
    }

    public void onClickOperation(View view) {
        btnOperation = (Button) view;
        String op = btnOperation.getText().toString();
        String stringHistory = txtHistory.getText().toString();
        String currentText = txtInput.getText().toString();

        inputValidation(currentText, op, txtInput);

        stringHistory = numberOne + op;
        txtHistory.setText(stringHistory);
    }

    public void inputValidation(String currentText, String op, TextView txtInput){
       if(currentText.isEmpty() || currentText.equals(defaultValue)){
           numberOne = defaultValue;
           txtInput.setText(numberOne);
       }
    }

    public List<String> getOperationsList(){
        return Stream.of(OperationsEnum.values())
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    /*public void onClickOperation(View view){
        btnOperation = (Button) view;
        String op = btnOperation.getText().toString();
        String stringHistory = txtHistory.getText().toString();

        for ( String operation : operationsList) {
            if(txtHistory.getText().toString().isEmpty()){
                if(operation.equals(op)){
                    numberOne = txtInput.getText().toString();
                    txtHistory.append(numberOne + op);
                    txtInput.setText(numberOne);
                    operationString = op;
                }
            }else{
                if(operation.equals(op)){

                    numberTwo = txtInput.getText().toString();
                    numberOne = stringHistory.substring(0, stringHistory.length()-1);

                    if(numberOne.equals(numberTwo)){
                        txtHistory.setText(stringHistory.substring(0, stringHistory.length() - 1) + op);
                        operationString = op;
                    }else{
                        if(stringHistory.charAt(stringHistory.length() - 1) == '='){
                            numberOne = txtInput.getText().toString();
                            txtHistory.setText(numberOne + op) ;
                            txtInput.setText(numberOne);
                            numberOne = "";
                            numberTwo = "";
                            operationString = op;
                        }else
                        {
                            resultadoFinal = conversorDeOperacaoECalcula(operationString) + op;
                            txtHistory.setText(resultadoFinal);
                            txtInput.setText(resultadoFinal.substring(0, resultadoFinal.length()-1));
                            numberTwo = "";
                            numberOne = "";
                            operationString = op;
                        }
                    }

                    }
                }
            }
        }*/

    public void  onClickBackspace(View view){
        String allTerm = txtInput.getText().toString();
        if(!allTerm.isEmpty()){
            StringBuffer sb= new StringBuffer(allTerm);
            sb.deleteCharAt(sb.length()-1);
            numberTwo = sb.toString();
            numberOne = sb.toString();
            txtInput.setText(sb);
        }
    }

    String conversorDeOperacaoECalcula(String op){
        Integer opInteiro = (int) op.charAt(0);
        Double n1 = Double.parseDouble(numberOne);
        Double n2 = Double.parseDouble(numberTwo);
        Double result = 0.0;

        if(opInteiro == 43)
            result = n1 + n2;

        if(opInteiro == 42)
            result = n1*n2;

        if(opInteiro == 45)
            result = n1-n2;

        if(opInteiro == 47){
            if(n2 == 0){
                txtInput.setText("Err");
            }else{
                result = n1/n2;
            }
        }
        return result.toString();
    }

    public void onClickC (View view){
        txtInput.setText("0");
        txtHistory.setText("");
        numberTwo = "";
        numberOne = "";
    }

    public void onClickEqual (View view){
        String stringHistory = txtHistory.getText().toString();
        String finalInput = "";

        if(stringHistory.charAt(stringHistory.length() - 1) == '='){
            numberOne = txtInput.getText().toString();
            finalInput = conversorDeOperacaoECalcula(operationString);
            txtInput.setText(finalInput);
        }else{
            numberTwo = txtInput.getText().toString();
            numberOne = stringHistory.substring(0, stringHistory.length()-1);
            resultadoFinal = numberOne + operationString + numberTwo + "=";
            finalInput = conversorDeOperacaoECalcula(operationString);
            txtInput.setText(finalInput);
            txtHistory.setText(resultadoFinal);


        }
    }

    public void  onClickDot(View view){
        String allTerm = txtInput.getText().toString();
        String stringHistory = txtHistory.getText().toString();
        txtInput.append(".");
        numberOne = txtInput.getText().toString();

    }
}