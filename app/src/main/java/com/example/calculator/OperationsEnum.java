package com.example.calculator;

public enum OperationsEnum {
    SOMA("+"),
    SUBTRACAO("-"),
    MULTIPLICACAO("*"),
    DIVISAO("/");

    private String value;

    OperationsEnum(String value) {
        value = value;
    }

}
